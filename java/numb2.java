import java.util.ArrayList;
import java.util.Arrays;

public class numb2 {
	public static void main(String[] args) {
		Integer[] nums = {1,2,3,4};
		int x = 4;
		System.out.println(Arrays.toString(func2(nums,x)));
	}
	static Integer[] func2(Integer[] nums,int x) {
		ArrayList<Integer> temp = new ArrayList<>();
		for(int i : nums) {
			boolean isSame = false;
			for(int j : nums) {
				if(i/j==x) {
					isSame = true;
					break;
				}
			}
			if(!isSame) {
				temp.add(i);
			}
		}
		Integer[] allnum = new Integer[temp.size()];
		allnum = temp.toArray(allnum);
		return allnum;
	}
}
