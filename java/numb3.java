import java.util.ArrayList;
import java.util.Arrays;

public class numb3 {
	public static void main(String[] args) {
		int x = 4;
		String word = "souvenir loud four lost";
		System.out.println(Arrays.toString(func3(word,x)));
	}
	static String[] func3(String word,int x) {
		ArrayList<String> temp = new ArrayList<>();
		String[] words = word.trim().split("\\s+");
		for(String i : words) {
			if(i.length()==x) {
				temp.add(i);
			}
		}
		String[] allword = new String[temp.size()];
		allword = temp.toArray(allword);
		return allword;
	}
}
