import java.util.ArrayList;
import java.util.Arrays;

public class numb1 {
	public static void main(String[] args) {
		Integer[] nums = {3,2,1,4};
		System.out.println(Arrays.toString(func1(nums)));
	}
	static Integer[] func1(Integer[] x) {
		ArrayList<Integer> temp = new ArrayList<>();
		for(int i : x) {
			boolean isUnder = false;
			for(int j : x) {
				if(i-j<0) {
					isUnder = true;
					break;
				}
			}
			if(!isUnder) {
				temp.add(i);
			}
		}
		Integer[] allnum = new Integer[temp.size()];
		allnum = temp.toArray(allnum);
		return allnum;
	}
}
