#include "numb3.h"

using ArrayList = java::util::ArrayList;
using Arrays = java::util::Arrays;

void numb3::main(std::vector<std::wstring> &args)
{
	int x = 4;
	std::wstring word = L"souvenir loud four lost";
	std::wcout << Arrays->toString(func3(word,x)) << std::endl;
}

std::vector<std::wstring> numb3::func3(const std::wstring &word, int x)
{
	std::vector<std::wstring> temp;
	std::vector<std::wstring> words = StringHelper::trim(word)->split(L"\\s+");
	for (auto i : words)
	{
		if (i.length() == x)
		{
			temp.push_back(i);
		}
	}
	std::vector<std::wstring> allword(temp.size());
	allword = temp.toArray(allword);
	return allword;
}
