#include "numb2.h"

using ArrayList = java::util::ArrayList;
using Arrays = java::util::Arrays;

void numb2::main(std::vector<std::wstring> &args)
{
	std::vector<std::optional<int>> nums = {1, 2, 3, 4};
	int x = 4;
	std::wcout << Arrays->toString(func2(nums,x)) << std::endl;
}

std::vector<std::optional<int>> numb2::func2(std::vector<std::optional<int>&> &nums, int x)
{
	std::vector<int> temp;
	for (auto i : nums)
	{
		bool isSame = false;
		for (auto j : nums)
		{
			if (i / j == x)
			{
				isSame = true;
				break;
			}
		}
		if (!isSame)
		{
			temp.push_back(i);
		}
	}
	std::vector<std::optional<int>> allnum(temp.size());
	allnum = temp.toArray(allnum);
	return allnum;
}
