#include "numb1.h"

using ArrayList = java::util::ArrayList;
using Arrays = java::util::Arrays;

void numb1::main(std::vector<std::wstring> &args)
{
	std::vector<std::optional<int>> nums = {3, 2, 1, 4};
	std::wcout << Arrays->toString(func1(nums)) << std::endl;
}

std::vector<std::optional<int>> numb1::func1(std::vector<std::optional<int>&> &x)
{
	std::vector<int> temp;
	for (auto i : x)
	{
		bool isUnder = false;
		for (auto j : x)
		{
			if (i - j < 0)
			{
				isUnder = true;
				break;
			}
		}
		if (!isUnder)
		{
			temp.push_back(i);
		}
	}
	std::vector<std::optional<int>> allnum(temp.size());
	allnum = temp.toArray(allnum);
	return allnum;
}
